/**
 * Выводит лог в зависимости от установки переменной окружения DEBUG
 *
 * @export
 * @param {string} item строка для вывода в лог
 */
export function debugLog(...items) {
  if (process.env.DEBUG === true || process.env.DEBUG === 'true') {
    console.log(...items);
  }
}

/** Возвращает url бакенда в зависимости от контекста
 * @returns {string}
 */
export function getAPIurl() {
  if (process.env.MODE === 'capacitor') {
    return (
      process.env.CAPACITOR_GRAPHQL_URI ||
      process.env.GRAPHQL_URI ||
      'http://localhost:8000/api/v1.1/'
    ).replace(/['"]/g, '');
  } else
    return (
      process.env.GRAPHQL_URI || 'http://localhost:8000/api/v1.1/'
    ).replace(/['"]/g, '');
}
