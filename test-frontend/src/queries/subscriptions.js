import gql from 'graphql-tag';

export const docsSubscriptions = gql`
  subscription docsSubscriptions($organizojUuid: [String]!) {
    dokumentoEkspedoEventoj(organizojUuid: $organizojUuid) {
      evento
      objekto {
        __typename
      }
      dokumento {
        uuid
        ekspedo {
          edges {
            node {
              uuid
            }
          }
        }
      }
    }
  }
`;
