import { RouteRecordRaw } from 'vue-router';
import { useCurrentUserStore } from 'src/stores/current-user';


const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'start',
    component: () => import('layouts/IndexLayout.vue'),
    beforeEnter:(to,from)=>{
      const store = useCurrentUserStore()
      console.log(store);
      if (store.getUserId) {
        return {name: 'projects'}
      }
      return
    },
  },
  {
    path: '/projects',

    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'projects',
        component: () => import('pages/ProjectsPage.vue'),
        beforeEnter:(to,from)=>{
          const store = useCurrentUserStore()
          console.log(store);
          if (!store.getUserId) {
            return {name: 'start'}
          }
          return
        },
      },
      {
        path: '/login',
        name: 'login',
        component: () => import('pages/LoginPage.vue'),
        beforeEnter:(to,from)=>{
          const store = useCurrentUserStore()
          console.log(store);
          if (store.getUserId) {
            return {name: 'projects'}
          }
          return
        },
      },
      {
        path: '/profile',
        name: 'profile',
        component: () => import('pages/ProfilePage.vue'),
        beforeEnter:(to,from)=>{
          const store = useCurrentUserStore()
          console.log(store);
          if (!store.getUserId) {
            return {name: 'start'}
          }
          return
        },
      },

    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
